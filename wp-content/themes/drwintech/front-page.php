<?php get_header();?>
<main class="bg-white">
    <section id="banner" class="relative overflow-hidden mb-12">
        <div class="container">
            <div class="flex flex-wrap py-12 border-b-2 border-drwintech-blue">
                <div class="lg:w-1/2 w-full">
                    <div class="rounded-r-[90px] rounded-bl-[90px] rounded-tl-none bg-drwintech-blue lg:py-28 lg:pl-20 lg:pr-24 p-8 relative">
                        <h1 class="text-4xl text-white font-medium mb-13">
                            Société spécialisée dans <br> les TICs :
                        </h1>
                        <p class="text-white text-3xl">
                            Conseils en technologie de <br>
                            l'information - Transformation <br>
                            numérique
                        </p>
                    </div>
                </div>
                <div class="lg:w-1/2 w-full">
                    <img class="" src="<?php echo get_theme_file_uri('assets/img/home_picture.png');?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
                </div>
            </div>
        </div>
        <div class="bg-white h-[400px] w-[400px] rounded-t-[300px] rounded-b-[300px] border-[45px] border-drwintech-blue absolute bottom-0 right-[-200px] lg:block hidden"></div>
    </section>

    <section id="missions" class="relative mb-20">
        <div class="container">
            <div class="flex flex-wrap lg:px-8 z-10">
                <div class="grid lg:grid-cols-3 grid-cols-1 gap-16 w-full">
                    <div class="bg-drwintech-gray pt-8 pb-18 pl-6 pr-10 rounded-xl">
                        <div class="flex flex-wrap items-center">
                            <div class="w-1/5">
                                <img class="h-8 mx-auto" src="<?php echo get_theme_file_uri('assets/img/rocket.png');?>" alt="rocket" title="rocket">
                            </div>
                            <div class="w-4/5">
                                <h2 class="text-drwintech-blue text-3xl font-bold mb-2">
                                    Notre mission
                                </h2>
                            </div>
                            <div class="w-1/5">

                            </div>
                            <div class="w-4/5">
                                <p class="">
                                    Analyser les besoins des entreprises <br>
                                    Trouver des solutions novatrices <br>
                                    Satisfaire rapidement et qualitativement la demande
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-drwintech-gray pt-8 pb-18 pl-6 pr-10 rounded-xl">
                        <div class="flex flex-wrap items-center">
                            <div class="w-1/5">
                                <img class="h-8 mx-auto" src="<?php echo get_theme_file_uri('assets/img/rocket.png');?>" alt="rocket" title="rocket">
                            </div>
                            <div class="w-4/5">
                                <h2 class="text-drwintech-blue text-3xl font-bold mb-2">
                                    Notre mission
                                </h2>
                            </div>
                            <div class="w-1/5">

                            </div>
                            <div class="w-4/5">
                                <p class="">
                                    Analyser les besoins des entreprises <br>
                                    Trouver des solutions novatrices <br>
                                    Satisfaire rapidement et qualitativement la demande
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-drwintech-gray pt-8 pb-18 pl-6 pr-10 rounded-xl">
                        <div class="flex flex-wrap items-center">
                            <div class="w-1/5">
                                <img class="h-8 mx-auto" src="<?php echo get_theme_file_uri('assets/img/rocket.png');?>" alt="rocket" title="rocket">
                            </div>
                            <div class="w-4/5">
                                <h2 class="text-drwintech-blue text-3xl font-bold mb-2">
                                    Notre mission
                                </h2>
                            </div>
                            <div class="w-1/5">

                            </div>
                            <div class="w-4/5">
                                <p class="">
                                    Analyser les besoins des entreprises <br>
                                    Trouver des solutions novatrices <br>
                                    Satisfaire rapidement et qualitativement la demande
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-drwintech-blue h-[226px] -mt-36 z-1 lg:block hidden">

            </div>

        </div>
    </section>

    <section id="services" class="relative mb-15">
        <div class="container">
            <h3 class="text-drwintech-blue text-3xl font-bold mb-2 text-center">
                Nos services
            </h3>
            <p class="text-center text-2xl">
                Nos services sont tous complémentaires et permettent à chacun de répondre à un besoin spécifique
            </p>

            <div class="flex flex-wrap mt-18">
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/pccare.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        Pc Care
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    Sécurité et maintenance de parc informatique
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <a href="pc-care" title="Pc-Care">
                                        <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl h-full">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/studio.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        Digital Studio
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    Conception et gestion de Portail Web
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/dnoc.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        DNoC
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    <span class="font-medium">
                                        Digital Network Operating Center
                                    </span>
                                    Gestion à distance de réseaux et surveillance
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/help.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        Help Desk
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    Gestion à distance de réseaux et surveillance
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/soc.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        SoC
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    <span class="font-medium">
                                        Security Operatin Center
                                    </span>
                                    Centre des Opérations de cybersécurité pour les Entreprises
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:w-1/3 w-full my-6 px-4">
                    <div class="bg-drwintech-blue rounded-xl">
                        <div class="pl-6 pt-8 pb-6">
                            <div class="flex flex-wrap items-center">
                                <div class="w-1/5">
                                    <img class="w-16 p-4 bg-drwintech-gray rounded-full" src="<?php echo get_theme_file_uri('assets/img/vcio.png');?>" alt="Pc Care" title="Pc Care">
                                </div>
                                <div class="w-4/5">
                                    <h2 class="text-white text-left text-3xl font-bold mb-2 ml-12">
                                        vCIO
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="p-[1px]">
                            <div class="bg-drwintech-gray rounded-xl h-[202px]">
                                <div class="pt-13 px-13 text-xl">
                                    <span class="font-medium">
                                        Virtual Chiel Information Officer
                                    </span>
                                    Directeur Informatique virtuel
                                </div>
                                <div class="flex px-7 justify-end mb-5">
                                    <img class="" src="<?php echo get_theme_file_uri('assets/img/arrow.png');?>" alt="" title="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="why-us" class="relative mb-8">
        <div class="container">
            <h3 class="text-drwintech-blue text-3xl font-bold mb-10 text-center">
                Pourquoi Nous ?
            </h3>
            <div class="bg-cover bg-center lg:pt-19 lg:px-14 lg:pb-40 p-6" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.5) 100%), url('<?php echo get_theme_file_uri('assets/img/whyus.png');?>')">
                <p class="text-white text-center mb-23 text-2xl">
                    Nous sommes une société internationale de conseil en technologies de l'information et de la communication. Drwintech est bien placée
                    pour être un co-innovateur des entreprises afin de réimaginer leurs activités grâce à des solutions  construites autour du numérique,
                    de l'IoT, du cloud, de l'automatisation, de la cybersécurité, de l'analyse, de la gestion de l'infrastructure et des services d'ingénierie.
                    Nous vous garantissons:
                </p>
                <div class="grid lg:grid-cols-2 grid-cols-1 gap-x-18 gap-y-10 w-full">
                    <div class="bg-white border-t-2 border-l-2 border-drwintech-blue hover:bg-drwintech-blue text-drwintech-blue hover:text-white text-center py-6 transition duration-300 text-xl">
                        Un personnel qualifié
                    </div>
                    <div class="bg-white border-t-2 border-l-2 border-drwintech-blue hover:bg-drwintech-blue text-drwintech-blue hover:text-white text-center py-6 transition duration-300 text-xl">
                        Une satisfaction rapide
                    </div>
                    <div class="bg-white border-t-2 border-l-2 border-drwintech-blue hover:bg-drwintech-blue text-drwintech-blue hover:text-white text-center py-6 transition duration-300 text-xl">
                        Un partenaire idéal
                    </div>
                    <div class="bg-white border-t-2 border-l-2 border-drwintech-blue hover:bg-drwintech-blue text-drwintech-blue hover:text-white text-center py-6 transition duration-300 text-xl">
                        Une accessibilité à tous
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact-us" class="relative">
        <div class="container">
            <h3 class="text-drwintech-blue text-3xl font-bold mb-10 text-center">
                Contactez-Nous ?
            </h3>
            <div class="flex flex-wrap">
                <div class="lg:w-1/2 w-full p-6">
                    <div class="border-2 border-drwintech-blue p-2">
                        <iframe class="w-full" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3324.330860298098!2d2.387814783706435!3d6.3643738164180315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x102355037a84343b%3A0xe55dffa56ff66fdd!2sDRWINTECH%20INC%20BENIN!5e0!3m2!1sfr!2sbj!4v1645533552027!5m2!1sfr!2sbj" width="" height="760" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                </div>
                <div class="lg:w-1/2 w-full px-6 pt-6">
                    <form action="">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Nom *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Prénoms *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Téléphone *">
                        <input type="email" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Email *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Nom de l'entreprise *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Comment avez-vous entendu parler de nous ? *">
                        <textarea type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5 h-[275px]" placeholder="Votre Message"></textarea>
                        <button class="w-full text-white bg-drwintech-blue px-4 py-3 rounded text-xl">
                            Envoyer
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer();?>
