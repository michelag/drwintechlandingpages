'use strict';

$(function() {
    //AOS
    AOS.init();

    // Javascript to toggle the menu
    $('#nav-toggle').click(function () {
        $('#menu-main-navigation:visible').hide(400);

        $('#menu-main-navigation:hidden').show(400);
    });

    $('.menu-item-has-children').mouseenter(function(){

        $(".menu-item-has-children .sub-menu").addClass('animate__animated animate__fadeIn');

    }).mouseleave(function(){
        $(".menu-item-has-children .sub-menu").removeClass('animate__animated animate__fadeIn');
    });

    $("#head header #logo a img").attr('src', '/drwintech/wp-content/themes/drwintech/assets/img/logo2.png');

    // var openmodal = document.querySelectorAll('.modal-open')
    // for (var i = 0; i < openmodal.length; i++) {
    //     openmodal[i].addEventListener('click', function (event) {
    //         event.preventDefault()
    //         toggleModal()
    //     })
    // }
    //
    // const overlay = document.querySelector('.modal-overlay')
    // overlay.addEventListener('click', toggleModal)
    //
    // var closemodal = document.querySelectorAll('.modal-close')
    // for (var i = 0; i < closemodal.length; i++) {
    //     closemodal[i].addEventListener('click', toggleModal)
    // }
    //
    // document.onkeydown = function (evt) {
    //     evt = evt || window.event
    //     var isEscape = false
    //     if ("key" in evt) {
    //         isEscape = (evt.key === "Escape" || evt.key === "Esc")
    //     } else {
    //         isEscape = (evt.keyCode === 27)
    //     }
    //     if (isEscape && document.body.classList.contains('modal-active')) {
    //         toggleModal()
    //     }
    // };
    //
    //
    // function toggleModal() {
    //     const body = document.querySelector('body')
    //     const modal = document.querySelector('.modal')
    //     modal.classList.toggle('opacity-0')
    //     modal.classList.toggle('pointer-events-none')
    //     body.classList.toggle('modal-active')
    // }


});