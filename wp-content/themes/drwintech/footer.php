<?php wp_footer(); ?>
<footer class="bg-drwintech-blue py-10 mt-8">
    <div class="container">
        <div class="flex flex-wrap">
            <div class="lg:w-1/3 w-full lg:mb-0 mb-6">
                <h4 class="text-3xl font-bold lg:mb-9 mb-3 text-white">
                    Nos contacts
                </h4>
                <ul class="inline-block text-white text-lg">
                    <li class="flex items-center mb-5">
                        <img class="w-8 mr-4" src="<?php echo get_theme_file_uri('assets/img/phone.png');?>" alt="" title="">
                        <span class="">+229 62 70 70 02 / 52 26 97 25</span>
                    </li>
                    <li class="flex items-center">
                        <img class="w-8 mr-4" src="<?php echo get_theme_file_uri('assets/img/mail.png');?>" alt="" title="">
                        <span class="">info@drwintech.com</span>
                    </li>
                </ul>
            </div>
            <div class="lg:w-1/3 w-full lg:mb-0 mb-6">
                <h4 class="text-3xl font-bold lg:mb-9 mb-3 text-white">
                    Nos médias sociaux
                </h4>
                <ul class="inline-block text-white text-lg">
                    <li class="flex items-center mb-5">
                        <img class="w-8 mr-4" src="<?php echo get_theme_file_uri('assets/img/facebook.png');?>" alt="" title="">
                        <span class="">DRWINTECH BENIN</span>
                    </li>
                    <li class="flex items-center">
                        <img class="w-8 mr-4" src="<?php echo get_theme_file_uri('assets/img/linkedin.png');?>" alt="" title="">
                        <span class="">DRWINTECH BENIN</span>
                    </li>
                </ul>
            </div>
            <div class="lg:w-1/3 w-full lg:mb-0 mb-6">
                <h4 class="text-3xl font-bold lg:mb-9 mb-3 text-white">
                    Situation géographique
                </h4>
                <p class="text-white text-lg pr-8">
                    Cotonou/ Aïbatin 1; 3ème
                    Rue après le CEG Houéyiho
                    en longeant la clôture de
                    l'Aéroport international CBG
                </p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>