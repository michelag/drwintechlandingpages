<?php get_header('pc-care');?>
<main class="bg-white">

    <section id="pc-care" class="relative my-22">
        <div class="container">
            <div class="flex flex-wrap">
                <div class="lg:w-3/5 w-full lg:pr-6 my-4">
                    <div class="border-t-5 border-l-5 border-b-5 border-drwintech-blue">
                        <h2 class="font-bold text-drwintech-blue text-5xl mb-8 p-6">
                            Qu'est-ce que Pc Care
                        </h2>
                        <p class="bg-white text-2xl mb-8 -ml-2 py-6 pr-4 leading-[30px]">
                            Pc Care est l’un des services que Drwintech vous propose. Comme l’indique son nom, Pc Care est une solution qui veille à l’entretien des parcs informatiques.
                            Le type de maintenance que nous vous proposons prend en compte la sécurité et l’optimisation de vos outils informatiques.
                        </p>
                        <h2 class="font-bold text-drwintech-blue text-5xl p-6">

                        </h2>
                    </div>
                </div>
                <div class="lg:w-2/5 w-full my-4">
                    <div class="frame">
                        <img class="w-full h-full object-cover object-center" src="<?php echo get_theme_file_uri('assets/img/whats.png');?>" alt="pc-care" title="pc-care">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="pc-care-security" class="relative my-22">
        <div class="container">
            <div class="flex flex-wrap">
                <div class="lg:w-3/5 w-full">
                    <div class="bg-drwintech-blue text-white text-5xl py-6 text-center font-bold">
                        PC Care Security
                    </div>
                </div>
            </div>
            <hr class="bg-drwintech-blue h-[4px] -mt-[1px]">
            <p class="text-xl my-10">
                Pc Care Security a deux essentielles fonctionnalités :  La sécurité et la gestion
            </p>
            <div class="grid lg:grid-cols-2 grid-cols-1 gap-14">
                <div class="shadow-fonctionnalites rounded-lg bg-white p-6">
                    <div class="border-5 border-drwintech-blue rounded-lg p-10 h-full">
                        <div class="flex items-center mb-5">
                            <img class="w-8 mr-3" src="<?php echo get_theme_file_uri('assets/img/rocket.png');?>" alt="" title="">
                            <h3 class="font-bold text-2xl text-drwintech-blue">
                                Fonctionnalités de sécurité
                            </h3>
                        </div>
                        <ul class="list-fonctionnalites text-lg">
                            <li>La protection contre les menaces ciblant les fichiers, le Web et les emails</li>
                            <li>La prévention des ransomwares et des exploits</li>
                            <li>L'analyse des vulnérabilités</li>
                            <li>La protection des appareils mobiles</li>
                            <li>La protection par mot de passe avec un mot de passe de déverouillage de Face ID et Touch ID</li>
                            <li>L'antivol permet de localiser à distance l'appareil</li>
                        </ul>
                    </div>
                </div>
                <div class="shadow-fonctionnalites rounded-lg bg-white p-6">
                    <div class="border-5 border-drwintech-blue rounded-lg p-10 h-full">
                        <div class="flex items-center mb-5">
                            <img class="w-8 mr-3" src="<?php echo get_theme_file_uri('assets/img/rocket.png');?>" alt="" title="">
                            <h3 class="font-bold text-2xl text-drwintech-blue">
                                Fonctionnalités de gestion
                            </h3>
                        </div>
                        <ul class="list-fonctionnalites text-lg">
                            <li>Blocage du cloud</li>
                            <li>Protection pour Microsoft Office 365</li>
                            <li>Endpoint Detection and Response</li>
                            <li>Contrôle du Web</li>
                            <li>Gestion des périphériques</li>
                            <li>Gestion du chiffrement</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="pc-care-optimizer" class="relative my-22">
        <div class="container">
            <div class="flex flex-wrap">
                <div class="lg:w-3/5 w-full">
                    <div class="bg-drwintech-blue text-white text-5xl py-6 text-center font-bold">
                        PC Care Optimizer
                    </div>
                </div>
            </div>
            <hr class="bg-drwintech-blue h-[4px] -mt-[1px]">
            <p class="text-xl my-10">
                La fonction optimisation consiste à améliorer l'efficacité d’un ordinateur en permettant au programme résultant de s'exécuter plus rapidement, de prendre moins de place en mémoire, de limiter la consommation de ressource.  Elle rend l’ordinateur plus performant.
                Elle est composée d’un nettoyeur d'espace de disque gratuit, un gestionnaire de confidentialité et un optimiseur de système informatique.
                Pc Care OPTIMIZER est conçu pour les systèmes Gnu / Linux, Windows et permet de nettoyer des milliers d'applications y compris les navigateurs, etc.
            </p>
            <div class="grid lg:grid-cols-2 grid-cols-1">
                <div class="">
                    <img class="w-full h-full object-center object-cover" src="<?php echo get_theme_file_uri('assets/img/careoptimizer.png');?>" alt="" title="">
                </div>
                <div class="bg-drwintech-blue pt-12 pr-6 pl-16 pb-17 h-full">
                    <h3 class="text-4xl font-bold text-white mb-20">
                        Autres fonctionnalités
                    </h3>
                    <p class="text-white text-xl leading-[48px]">
                        Pc Care Optimizer permet de :
                        Disposer d’un système d’alerte précoce robuste contre les défaillances de système
                        Disposer d’une page d'accueil avec plusieurs outils d'optimisation
                        Détruire des fichiers et nettoyer l'espace disque
                        Prendre en charge complète du démarrage sécurisé
                        Identifier et  supprimer du cache Web , des cookies HTTP , de l’historique des URL , des fichiers journaux temporaires et des cookies
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="nos-offres" class="relative my-22 bg-cover bg-center pt-8 pb-20" style="background-image: url('<?php echo get_theme_file_uri('assets/img/pict.jpg');?>')">
        <div class="container">
            <h3 class="text-5xl font-bold text-drwintech-blue mb-7 text-center">
                Nos offres
            </h3>
            <p class="text-2xl text-center mb-12">
                Choisissez votre pack et contactez-nous maintenant
            </p>
            <div class="grid lg:grid-cols-3 grid-cols-1 gap-8">
                <div class="bg-white rounded-2xl border-b-6 border-drwintech-blue pb-16">
                    <div class="bg-drwintech-blue py-8 px-6 rounded-2xl relative">
                        <h4 class="text-3xl text-white text-center font-bold mb-1">
                            Pc Care Standard
                        </h4>
                        <h5 class="text-xl text-white text-center mb-1">
                            1-20 Postes
                        </h5>
                        <h6 class="text-xl font-semibold text-white text-center">
                            3000 XOF/Mois
                        </h6>
                        <div class="triangle-down"></div>
                    </div>
                    <div class="pt-8 px-6">
                        <ul class="list-offers mb-20">
                            <li class="">Profil de sécurité pré-configuré</li>
                            <li class="">Fonctionnalités de protection</li>
                            <li class="">Protection contre les ransomwares</li>
                            <li class="">Protection des appareils mobiles</li>
                            <li class="">Evalution de la vulnarébilité</li>
                            <li class="">Mise à jour régulière et automatique</li>
                        </ul>
                        <div class="flex justify-center items-center">
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded-2xl border-b-6 border-drwintech-blue pb-16">
                    <div class="bg-drwintech-blue py-8 px-6 rounded-2xl relative">
                        <h4 class="text-3xl text-white text-center font-bold mb-1">
                            Pc Care Pro
                        </h4>
                        <h5 class="text-xl text-white text-center mb-1">
                            Plus de 20 Postes
                        </h5>
                        <h6 class="text-xl font-semibold text-white text-center">
                            5000 XOF/Mois
                        </h6>
                        <div class="triangle-down"></div>
                    </div>
                    <div class="pt-8 px-6">
                        <h4 class="text-xl text-drwintech-blue font-bold mb-2 ml-6">
                            Pc Care Standard +
                        </h4>
                        <ul class="list-offers mb-12">
                            <li class="">Profil de sécurité pré-configuré</li>
                            <li class="">Fonctionnalités de protection</li>
                            <li class="">Protection contre les ransomwares</li>
                            <li class="">Protection des appareils mobiles</li>
                            <li class="">Evalution de la vulnarébilité</li>
                            <li class="">Mise à jour régulière et automatique</li>
                        </ul>
                        <div class="flex justify-center items-center">
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full mr-4">
                            </div>
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded-2xl border-b-6 border-drwintech-blue pb-16">
                    <div class="bg-drwintech-blue py-8 px-6 rounded-2xl relative">
                        <h4 class="text-3xl text-white text-center font-bold mb-1">
                            Pc Care Standard
                        </h4>
                        <h5 class="text-xl text-white text-center mb-1">
                            1-20 Postes
                        </h5>
                        <h6 class="text-xl font-semibold text-white text-center">
                            3000 XOF/Mois
                        </h6>
                        <div class="triangle-down"></div>
                    </div>
                    <div class="pt-8 px-6">
                        <h4 class="text-xl text-drwintech-blue font-bold mb-2 ml-6">
                            Pc Care Entreprise +
                        </h4>
                        <ul class="list-offers mb-12">
                            <li class="">Profil de sécurité pré-configuré</li>
                            <li class="">Fonctionnalités de protection</li>
                            <li class="">Protection contre les ransomwares</li>
                            <li class="">Protection des appareils mobiles</li>
                            <li class="">Evalution de la vulnarébilité</li>
                            <li class="">Mise à jour régulière et automatique</li>
                        </ul>
                        <div class="flex justify-center items-center">
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full mr-4">
                            </div>
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full mr-4">
                            </div>
                            <div class="w-12 h-12 bg-drwintech-blue rounded-full">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact-us" class="relative">
        <div class="container">
            <h3 class="text-drwintech-blue text-3xl font-bold mb-10 text-center">
                Contactez-Nous ?
            </h3>
            <div class="flex flex-wrap">
                <div class="lg:w-1/2 w-full p-6">
                    <div class="border-2 border-drwintech-blue p-2">
                        <iframe class="w-full" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3324.330860298098!2d2.387814783706435!3d6.3643738164180315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x102355037a84343b%3A0xe55dffa56ff66fdd!2sDRWINTECH%20INC%20BENIN!5e0!3m2!1sfr!2sbj!4v1645533552027!5m2!1sfr!2sbj" width="" height="760" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                </div>
                <div class="lg:w-1/2 w-full px-6 pt-6">
                    <form action="">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Nom *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Prénoms *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Téléphone *">
                        <input type="email" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Email *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Nom de l'entreprise *">
                        <input type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5" placeholder="Comment avez-vous entendu parler de nous ? *">
                        <textarea type="text" id="" class="px-4 py-3 w-full border border-drwintech-blue rounded outline-none mb-5 h-[275px]" placeholder="Votre Message"></textarea>
                        <button class="w-full text-white bg-drwintech-blue px-4 py-3 rounded text-xl">
                            Envoyer
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer();?>
