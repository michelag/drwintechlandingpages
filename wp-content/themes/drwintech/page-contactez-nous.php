<?php get_header() ?>
<?php if (have_posts()): ?>
	<?php while(have_posts()): the_post(); ?>
		<?php /* grab the url for the full size featured image */
		$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
        <main class="">
            <section class="" id="banner">
                <img src="<?php echo get_theme_file_uri('assets/img/bigL.png');?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" class="mx-auto">
            </section>

            <div class="my-4 container">
<!--                <img src="--><?php //echo $featured_img_url;?><!--" alt="" class="h-112 object-cover object-center w-full mb-4">-->
<!--                <h1 class="font-bold text-3xl mb-4">-->
<!--					--><?php //the_title(); ?>
<!--                </h1>-->
                <div class="grid lg:grid-cols-2 grid-cols-1 gap-12">
                    <div class="py-4">
			            <?php the_content(); ?>
                    </div>
                    <div class="flex items-baseline lg:mt-16">
                        <div class="">
                            <h2 class="font-bold text-2xl underline mb-3">
                                Contact
                            </h2>
                            <h3 class="font-semibold text-xl mb-1">
                                Où nous trouver ?
                            </h3>
                            <p class="mb-4">
                                1624, Zone de recasement II, Almadies. <br>
                                368, Cité Touba Almadies <br>
                                DAKAR
                            </p>
                            <h3 class="font-semibold text-xl mb-1">
                                Nous écrire ?
                            </h3>
                            <p class="mb-4">
                                Email: contact@lebois.sn
                            </p>
                            <h3 class="font-semibold text-xl mb-1">
                                Nous parler ?
                            </h3>
                            <p class="mb-8">
                                Tel: +221 77 312 13 13
                            </p>
                            <h3 class="font-semibold text-xl mb-2">
                                Suivez sur nos réseaux sociaux:
                            </h3>
                            <ul class="inline-flex items-center">
                                <li class="mr-4">
                                    <a class="text-green-500 transition transform hover:-translate-y-1" target="_blank" href="https://wa.me/+221773121313" title="Lien WhatsApp">
                                        <i class="fi-xnsuxl-whatsapp"></i>
                                    </a>
                                </li>
                                <li class="mr-4">
                                    <a class="text-blue-400 transition transform hover:-translate-y-1" target="_blank" href="https://twitter.com/AktelisB" title="Lien Twitter">
                                        <i class="fi-xnsuxl-twitter-solid"></i>
                                    </a>
                                </li>
                                <li class="mr-4">
                                    <a class="text-blue-500 transition transform hover:-translate-y-1" target="_blank" href="https://fb.me/aktelisbois" title="Lien Facebook">
                                        <i class="fi-snsuxl-facebook"></i>
                                    </a>
                                </li>
                                <li class="mr-4">
                                    <a class="text-pink-400 transition transform hover:-translate-y-1" target="_blank" href="https://www.instagram.com/aktelisbois/" title="Lien Instagram">
                                        <i class="fi-xnsuxl-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>


            </div>
        </main>
	<?php endwhile ?>
<?php endif; ?>
<?php get_footer() ?>