<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?php echo get_theme_file_uri('assets/img/logo.png');?>" type="image/png" />
	<!--    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>-->
	<?php wp_head(); ?>
</head>
<section id="head" class="relative bg-center bg-cover h-pc-care h-auto" style="background-image: url('<?php echo get_theme_file_uri('assets/img/pccarebanner.png');?>')">
    <header id="" class="shadow-lg bg-white pt-8">
        <div class="flex flex-wrap justify-between container">
            <div id="logo" class="lg:order-1 order-1">
                <a href="<?php echo home_url();?>" title="<?php bloginfo('name');?>"><img src="<?php echo get_theme_file_uri('assets/img/logo.png');?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"></a>
            </div>
            <div class="block lg:hidden lg:order-3 order-3 flex justify-center items-center">
                <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-white border-white">
                    <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
                </button>
            </div>
            <nav class="lg:w-auto w-full lg:my-0 my-4 lg:flex lg:order-2 order-3">
                <?php wp_nav_menu(['theme_location' => 'header',
                                   'container' => false,
                                   'menu_class' => 'w-full lg:block hidden flex-grow lg:flex lg:flex-wrap lg:items-center lg:w-auto'
                ]) ?>
            </nav>
        </div>
    </header>
    <div class="container mt-44">
        <h1 class="text-6xl text-white font-bold mb-13">
            PC Care
        </h1>
        <p class="text-white text-4xl">
            Service d'optimisation et de <br>
            sécurité des parcs informatiques
        </p>
    </div>
    <img class="absolute bottom-[6%] left-[18%]" src="<?php echo get_theme_file_uri('assets/img/security.png');?>" alt="" title="">
</section>
<body>