<?php
function drwintech_supports()
{
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('menus');
	register_nav_menu('header', 'Header Menu | Main Navigation');
}

function drwintech_register_assets()
{
	wp_register_style('tailwind', get_theme_file_uri('assets/css/main.css'), ['aos', 'owl-carousel', 'owl-carousel-theme', 'animate-css'], false);
	wp_register_style('aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css', false);
	wp_register_style('owl-carousel', get_theme_file_uri('assets/owlcarousel/css/owl.carousel.min.css'), [], false);
	wp_register_style('owl-carousel-theme', get_theme_file_uri('assets/owlcarousel/css/owl.theme.default.min.css'), [], false);
	wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css', [], false);

//    wp_register_script('jquery','https://code.jquery.com/jquery-3.6.0.min.js');
	wp_register_script('owl-carousel',get_theme_file_uri('assets/owlcarousel/js/owl.carousel.min.js'), [], false);
	wp_register_script('popper','https://unpkg.com/@popperjs/core@2.9.1/dist/umd/popper.min.js', [], false);
	wp_register_script('friconix','https://friconix.com/cdn/friconix.js', [], false);
	wp_register_script('aos-js','https://unpkg.com/aos@2.3.1/dist/aos.js', [], false);
	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', [], false);
	wp_register_script('script',get_theme_file_uri('assets/js/script.js'), ['jquery', 'owl-carousel', 'popper', 'friconix', 'aos-js'], false);
	wp_enqueue_style('tailwind');
	wp_enqueue_script('script');
}

function drwintech_title_separator()
{
	return '|';
}

//function drwintech_document_title_parts($title)
//{
////    unset($title['tagline']);
//    return $title;
//}

function drwintech_menu_class($classes){
	$classes[] = 'mx-6 lg:my-0 my-3';
	return $classes;
}

function drwintech_menu_link_class($attrs){
	$attrs['class'] = 'text-drwintech-blue transition duration-700 font-bold text-lg';
	return $attrs;
}

function drwintech_init(){

	$labels = [
		// Le nom au pluriel
		'name'                => _x( 'Services', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Service', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Services'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les Services'),
		'view_item'           => __( 'Voir les Services'),
		'add_new_item'        => __( 'Ajouter une nouvelle Service'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer le Service'),
		'update_item'         => __( 'Modifier le Service'),
		'search_items'        => __( 'Rechercher une Service'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	];
	$args= [
		'label' => 'Service',
		'labels' => $labels,
		'public' => true,
		'menu_position' => 3,
		'menu_icon' => 'dashicons-camera-alt',
		'supports' => ['title', 'editor', 'thumbnail'],
		'show_in_rest' => true,
		'has_archive' => true,
	];

	register_post_type('services', $args);

	// Taxonomie Année

	// On déclare ici les différentes dénominations de notre taxonomie qui seront affichées et utilisées dans l'administration de WordPress
	$labels_categ = array(
		'name'              			=> _x( 'Catégories De Services', 'taxonomy general name'),
		'singular_name'     			=> _x( 'Catégorie De Services', 'taxonomy singular name'),
		'search_items'      			=> __( 'Chercher une Catégorie De Services'),
		'all_items'        				=> __( 'Toutes les Catégories De Services'),
		'edit_item'         			=> __( 'Editer la Catégorie De Services'),
		'update_item'       			=> __( 'Mettre à jour la Catégorie De Services'),
		'add_new_item'     				=> __( 'Ajouter une nouvelle Catégorie De Services'),
		'new_item_name'     			=> __( 'Valeur de la Catégorie De Services'),
		'separate_items_with_commas'	=> __( 'Séparer les réalisateurs avec une virgule'),
		'menu_name'         => __( 'Catégories De Services'),
	);

	$args_categ = array(
		// Si 'hierarchical' est défini à false, notre taxonomie se comportera comme une étiquette standard
		'hierarchical'      => true,
		'labels'            => $labels_categ,
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
//		'rewrite'           => array( 'slug' => 'annees' ),
	);

	register_taxonomy( 'categ_services', 'services', $args_categ );
}

require_once 'widgets/Drwintech.php';

function drwintech_register_widget(){
	register_widget(Drwintech::class);

	register_sidebar([
		'id' => 'homepage_banner',
		'name' => 'Homepage Banner',
		'before_widget' => '<div class="bg-drwintech-lightwhite bg-opacity-86 rounded-xl lg:px-9 lg:py-9 px-4 py-6 %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h1 class="font-bold text-5xl text-drwintech-brown mb-6">',
		'after_title' => '</h1>',
		'before_text' => '<p class="text-lg text-justify mb-6 font-light">',
		'after_text' => '</p>',
	]);
}

add_action('init', 'drwintech_init');
add_action('after_setup_theme', 'drwintech_supports');
add_action('wp_enqueue_scripts', 'drwintech_register_assets');
add_action('document_title_separator', 'drwintech_title_separator');
add_action('widgets_init', 'drwintech_register_widget');
//add_action('document_title_parts', 'drwintech_document_title_parts');
add_filter('nav_menu_css_class', 'drwintech_menu_class');
add_filter('nav_menu_link_attributes', 'drwintech_menu_link_class');